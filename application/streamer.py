import json
from tweepy.streaming import StreamListener
from tweepy import Stream
import time
import string


class MyListener(StreamListener):
    """Custom StreamListener for streaming data."""
    def __init__(self, data_dir, query, qfilter, start_time, timeout):
        self.time = start_time
        self.limit = int(timeout)
        self.filter = qfilter
        query_fname = format_filename(query)
        self.outfile = "%s/stream_%s.json" % (data_dir, query_fname)

    def on_data(self, data):
        while(time.time() - self.time) < self.limit:
            try:
                with open(self.outfile, 'a') as f:
                    if self.filter in data:
                        f.write(data)
                        print(data)
                        return True
                    else:
                        print(data)
            except BaseException as e:
                print("Error on_data: %s" % str(e))
                time.sleep(5)
            return True
        print("Stopped after %d seconds" % self.limit)
        exit(1)

    def on_error(self, status):
        print(status)
        return True


def format_filename(fname):
    """Convert file name into a safe string.
    Arguments:
        fname -- the file name to convert
    Return:
        String -- converted file name
    """
    return ''.join(convert_valid(one_char) for one_char in fname)


def convert_valid(one_char):
    """Convert a character into '_' if invalid.
    Arguments:
        one_char -- the char to convert
    Return:
        Character -- converted char
    """
    valid_chars = "-_.%s%s" % (string.ascii_letters, string.digits)
    if one_char in valid_chars:
        return one_char
    else:
        return '_'


@classmethod
def parse(cls, api, raw):
    status = cls.first_parse(api, raw)
    setattr(status, 'json', json.dumps(raw))
    return status


# Main entry point for twitter streamer
def main(auth, args):
    start_time = time.time()
    twitter_stream = Stream(auth, MyListener(args.data_dir, args.query, args.filter, start_time, timeout=args.timeout))
    twitter_stream.filter(track=[args.query])


if __name__ == '__main__':
    main()
