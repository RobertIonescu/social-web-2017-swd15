#
# Tool to get information from Twitter
# Todo describe programm more in detail
#
# Authors: Robert Ionescu | Sandjar Berdiev
# Date : 06.10.2017
# Version : 0.1
# Example call: python3.exe main.py -q "Donald Trump" -f "Atom Bomb" -d data -t 6


# Imports

from configuration import config
import os
import shutil
import tweepy
import sys
import argparse
from application import streamer
from threading import Thread


def get_parser():
    """Get parser for command line arguments."""
    parser = argparse.ArgumentParser(description="Twitter Downloader")
    parser.add_argument("-q",
                        "--query",
                        dest="query",
                        help="Query/Filter",
                        default='-')
    parser.add_argument("-d",
                        "--data-dir",
                        dest="data_dir",
                        help="Output/Data Directory")
    parser.add_argument("-f",
                        "--filter",
                        dest="filter",
                        help="Query filter")
    parser.add_argument("-t",
                        "--timeout",
                        dest="timeout",
                        help="Runtime in Seconds")
    return parser


# Main entry point for application
def main():
    # Verifying installed python version
    if sys.version_info[0] < 3:
        print("This APP requires Python 3")
        exit(1)

    print("Initializing...")

    # Parsing arguments with argparser module
    parser = get_parser()
    args = parser.parse_args()

    # Using OAuth to authenticate and access API using access tokens from config.py
    auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
    auth.set_access_token(config.access_token, config.access_secret)
    api = tweepy.API(auth)

    # Cleaning old entries
    print("Cleaning old files...")
    try:
        shutil.rmtree("data")
    except OSError:
        print("Nothing to remove")
        pass

    # Creating output destination
    print("Creating new output directory...")
    try:
        os.mkdir("data")
    except OSError:
        print("ERROR: Could not create new directory!")
        exit(1)

    # Starting streamer in new thread
    Thread(target=streamer.main(auth, args))


if __name__ == '__main__':
    main()
