# README #

This project contains a twitter app using the twitter API.
The application is going to:

* Collect
* Analyse
* Filter
* Store


Application usage:
    
    App call: main.py -q "Query" -f "Filter" -t "Timeout in Seconds"

Collecting: 
* search for query global twitter tweets
* matching tweets with filter
* saving matched tweets into json file


### What is this repository for? ###

* Twitter Application
* SocWeb Ionescu, Berdiev
* Version 1.0

### How do I get set up? ###

Required software:
* Python 3.6+ installation (https://www.python.org/ftp/python/3.6.3/python-3.6.3.exe)
* pip - bundled with python installation (verify with "pip --help")

Setting up environment according to requirements.txt

    pip install -r requirements.txt
